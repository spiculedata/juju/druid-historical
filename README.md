# Overview

Welcome to the Druid Historical charm by Spicule. Historical Nodes encapsulate the functionality to load and serve the immutable blocks of data(segments) 
created by real-time nodes. Most of the data in Druid is immutable and typically Historical Nodes are the main workers in the cluster.
For more information on Druid Historical, please visit the [Druid Historical Documentation](http://druid.io/docs/latest/design/historical.html).

# Usage

To deploy this charm from the command line, enter the following:

    juju deploy cs:~spiculecharms/druid-historical --series xenial

You can specify hardware constraints for your Historical node on the command 
line. To find out more, refer to the ["Using Constraints"](https://docs.jujucharms.com/2.4/en/charms-constraints").
For example, to deploy your Historical node on a server with 4 cores and 15 GB
of RAM, enter the following command:

    juju deploy cs:~spiculecharms/druid-historical --series xenial --constraints "mem=15G cores=4"

# OpenJDK and Druid Config

Druid Historical requires a relation to the [OpenJDK charm](https://jujucharms.com/openjdk/)
to automate the installation of Java onto Historicals server, which is a
requirement of Historical.

Additionally, Historical requires configuration as part of the wider Druid
cluster, so Historical must be related to our [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).
The individual Druid nodes do not need to be directly related to one another,
and should instead be directly related to Druid Config instead. 

# Configuration

Druid Cluster configuration is handled through the [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).

# Contact Information

Thank you for using the Druid Historical charm - we hope you find it useful! For
more information, here are our contact details:

Spicule: [http://www.spicule.co.uk](http://www.spicule.co.uk)  
Anssr: [http://anssr.io](http://anssr.io)  
Email: info@spicule.co.uk  

For assistance with Juju, please refer to the [Juju Discourse](https://discourse.jujucharms.com/).

## Druid

For more information about Druid, please refer to the [Druid website](http://druid.io).

