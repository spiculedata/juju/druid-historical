import math
import os
import stat
from subprocess import run as sub_run
from psutil import virtual_memory
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log
from subprocess import check_call


@when_not('druid-historical.installed')
def install_druid_historical():
    memory_check = check_sufficient_memory()
    if not memory_check[0]:
        log('Can not start Historical, server has ' +
            memory_check[1] + 'GB of RAM, but Historical requires at least ' + memory_check[2] + 'GB')
        status_set('blocked', 'Historical has insufficient RAM '
                              '(has ' + memory_check[1] + 'GB, requires ' + memory_check[2] + 'GB')
    else:
        archive = resource_get("druid")
        if not os.path.isdir('/opt/druid_historical'):
            os.mkdir('/opt/druid_historical')
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_historical', '--strip', '1']
        check_call(cmd)

        archive = resource_get("mysql-extension")
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_historical/extensions']
        check_call(cmd)

        os.mkdir('/opt/druid_historical/var')
        os.mkdir('/opt/druid_historical/var/tmp')
        os.mkdir('/opt/druid_historical/var/druid')
        os.mkdir('/opt/druid_historical/var/druid/indexing-logs')
        os.mkdir('/opt/druid_historical/var/druid/segments')
        os.mkdir('/opt/druid_historical/var/druid/segment-cache')
        os.mkdir('/opt/druid_historical/var/druid/task')
        os.mkdir('/opt/druid_historical/var/druid/hadoop-tmp')
        os.mkdir('/opt/druid_historical/var/druid/pids')
        os.mkdir('/opt/druid_historical/log')

        render('druid_historical', '/etc/init.d/druid_historical')
        render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
        st = os.stat('/etc/init.d/druid_historical')
        os.chmod('/etc/init.d/druid_historical', st.st_mode | stat.S_IEXEC)

        set_flag('druid-historical.installed')
        status_set('waiting', 'Waiting for config file')


@when('druid-historical.installed', 'endpoint.config.new_config')
def configure_druid_historical():
    with open('/opt/druid_historical/conf/druid/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()

    if old_conf != new_conf:
        log('New config detected! Resetting Druid Historical.')
        if is_flag_set('druid-historical.configured'):
            clear_flag('druid-historical.configured')

        status_set('maintenance', 'Configuring Historical')

        config_file = open('/opt/druid_historical/conf/druid/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        # write_runtime_properties()
        write_jvm_config()

        set_flag('druid-historical.configured')
        set_flag('druid-historical.new_config')
        status_set('maintenance', 'Druid Historical configured, waiting to start process...')


@when('druid-historical.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-historical.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_historical/conf/druid/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_historical/conf/druid/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_historical/conf/druid/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_historical/conf/druid/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-historical.configured'):
        clear_flag('druid-historical.configured')

    set_flag('druid-historical.hdfs_configured')
    set_flag('druid-historical.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')


@when_any('druid-historical.configured', 'druid-historical.hdfs_configured')
@when('java.ready', 'druid-historical.new_config')
def run_druid_historical(java):
    restart_historical()
    clear_flag('druid-historical.new_config')


def start_historical():
    status_set('maintenance', 'Starting Historical...')
    code = sub_run(['/etc/init.d/druid_historical', 'start'], cwd='/opt/druid_historical')
    if code.returncode == 0:
        set_flag('druid-historical.running')
        status_set('active', 'Historical running')
    else:
        status_set('blocked', 'Error starting Historical')


def stop_historical():
    status_set('maintenance', 'Stopping Historical...')
    code = sub_run(['/etc/init.d/druid_historical', 'stop'], cwd='/opt/druid_historical')
    if code.returncode == 0:
        clear_flag('druid-historical.running')
        status_set('waiting', 'Historical stopped.')
    else:
        status_set('blocked', 'Error stopping Historical')


def restart_historical():
    status_set('maintenance', 'Restarting Historical...')
    stop_historical()
    start_historical()


def check_minimum_memory():
    """
    Calculates the minimum memory requirement for this Druid Historical instance. Rule of thumb is:

            Minimum Requirement (bytes) = sizeBytes * (numThreads + numMergeBuffers + 1)

    However, we provide an extra GB of headroom on the low end to ensure that Druid Historical will be able to run.
    This additional GB is added within the return statement.

    :return: The minimum memory requirements (essentially Xms) of this Druid instance in bytes.
    """

    if os.cpu_count() > 1:
        numThreads = os.cpu_count() - 1
    else:
        numThreads = 1

    # numMergeBuffers is max of 2, numThreads/4
    if numThreads // 4 > 2:
        numMergeBuffers = numThreads // 4
    else:
        numMergeBuffers = 2

    sizeBytes = 1073741824  # 1 GB, parametize this later

    return sizeBytes * (numMergeBuffers + numThreads + 1) + 1073741824


def write_runtime_properties():

    context = {
        'mdms': check_minimum_memory()
    }

    render('runtime.properties', '/opt/druid_historical/conf/druid/historical/runtime.properties', context)


def write_jvm_config():

    mem_gb = check_system_memory() // (1024 ** 3)  # Total memory of server, floor division for additional overhead
    Xms = math.ceil(check_minimum_memory() / (1024 ** 3))  # Minimum memory requirement for Druid in GB
    Xmx = max(Xms, math.floor(mem_gb - 1))  # Allocate maximum memory from total, subtract 1GB for overhead or set Xms

    context = {
        'Xms': Xms,
        'Xmx': Xmx
    }

    render('jvm.config', '/opt/druid_historical/conf/druid/historical/jvm.config', context)


def check_system_memory():
    """
    Returns the total system memory in bytes.
    """

    mem = virtual_memory()
    return mem.total


def check_sufficient_memory():
    """
    This method checks to make sure there is sufficient memory before allowing the charm to continue installing.
    """

    # Get minimum memory requirement for the heap. Round up since we'll round up when allocating Xms
    heap = math.ceil(check_minimum_memory() // (1024 ** 3))

    # Get total memory of the system
    mem = check_system_memory() // (1024 ** 3)

    # If the memory requirements exceed the amount available
    if heap > mem:
        return False, str(mem), str(int(heap))
    else:
        return True, 0, 0
